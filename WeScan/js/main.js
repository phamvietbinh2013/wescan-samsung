$(function() {

    var mChannel = 'com.mrkaz.wescan';

    var mClientId = null;
    var fullscreen = false;
    var currentCookie = ""

    var player = videojs(document.querySelector('.video-js'));
    initHome();


    msf.local(function(err, service) {
        var reqAppControl = tizen.application.getCurrentApplication().getRequestedAppControl();

        if (reqAppControl && reqAppControl.appControl) {
            var data = reqAppControl.appControl.data;

            for (var i = 0; i < data.length; i++) {
                log("WRT param #" + i + " key : " + data[i].key);
                for (var j = 0; j < data[i].value.length; j++) {
                    console.log("WRT param #" + i + " #" + j + " value : " + data[i].value[j]);
                }
                if (data[i].key == "PAYLOAD") {
                    log("payload =" + data[i].value[0]);
                    var payload = data[i].value[0];
                }
            }
        }

        var channel = service.channel(mChannel);
        channel.connect({
            name: 'TV'
        }, function(err) {
            if (err) {
                return console.log(err);
            }
            log('channel.connect');
        });

        channel.on('connect', function(client) {
            log("connect client.id :" + client.id);
            console.log("connect channel.isConnected :" + channel.isConnected);
            console.log("connect channel.isConnected :" + channel.clients.length);

            if (channel.clients.length > 1) {
                log("availabe device");
            }
        });

        channel.on('disconnect', function(client) {
            log("disconnect ");
        });

        channel.on('clientConnect', function(client) {
            log("clientConnect ");

            if (channel.clients.length > 1) {
                log("availabe client");
            }

            mClientId = client.id;
            channel.publish('connect', 'clientConnect ' + client.attributes.name, client.id);
            title.innerHTML = "Connect " + client.attributes.name;

        });

        channel.on('clientDisconnect', function(client) {
            log("clientDisconnect ");
            channel.publish('disconnect', 'disconnect ' + client.attributes.name, client.id);
            title.innerHTML = "Disconnected";
        });

        channel.on('play', function(obj, from) {
            title.innerHTML = "Title : " + obj.videoTitle;
            readyPlayer(obj);
            channel.publish('play_TV', 'playing ' + obj.videoTitle, mClientId);
        });

        channel.on('control', function(msg, from) {
            log("event control : msg " + msg);
            if (msg == 'play')
                play();
            else if (msg == 'pause')
                puase();
            else if (msg == 'stop')
                stop();
            else if (msg == 'ff')
                ff();
            else if (msg == 'rew')
                rew();
            else if (msg == 'fullScreen') {
                fullscreen = true;
            } else if (msg == 'originScreen') {
                fullscreen = false;
            }
            channel.publish('contorl_TV', msg, mClientId);
        });

    });

    function log(msg) {
        console.log("" + msg)

    }

    function onPlayerStateChange(event) {
        switch (event.data) {
            case YT.PlayerState.ENDED:
                log('Video has ended.');
                break;
            case YT.PlayerState.PLAYING:
                log('Video is playing.');
                break;
            case YT.PlayerState.PAUSED:
                log('Video is paused.');
                break;
            case YT.PlayerState.BUFFERING:
                log('Video is buffering.');
                break;
            case YT.PlayerState.CUED:
                log('Video is cued.');
                break;
        }
    }

    function play() {
        player.play();
    }

    function stop() {
        player.stopVideo();
    }

    function puase() {
        player.pauseVideo();
    }

    function ff() {
        var ct = player.getCurrentTime();
        player.seekTo(ct + 10, true);
    }

    function rew() {
        var ct = player.getCurrentTime();
        player.seekTo(ct - 10, true);
    }


    function displayDiv(idMyDiv) {
        document.getElementById(idMyDiv).style.display = "block";
    }

    function hideDiv(idMyDiv) {
        document.getElementById(idMyDiv).style.display = "none";
    }

    function backToHome() {
        hideDiv("video-page");
        displayDiv("home-page");
    }

    function readyPlayer(obj) {
        window.xhook.before(function(request) {
            request.headers = request.headers || {};
            request.headers.Cookie = currentCookie
            log(request.url)
            log(request.headers.Cookie)
        });
        currentCookie = obj.authContent;
        player.src({
            src: obj.videoUrl,
            type: obj.mimeType
        })
        player.poster(obj.videoThumbnail)
        player.play()
    }

    function updatePlayerContent(movie) {
        player.poster(movie.thumbnail)
    }

    function initHome() {
        var request = new XMLHttpRequest();

        request.open('POST', 'https://staging.wescan.vn/api/v1/users/login/anonymous', true);

        request.onload = function() {
            var data = JSON.parse(this.response);

            if (request.status >= 200 && request.status < 400) {
                var token = data.data.token;
                log(token);

                loadVideos(token);

            } else {
                log("error");
            }
        }
        request.send();
    }

    function loadVideos(token) {
        var request = new XMLHttpRequest();
        request.open('GET', 'https://vs-api.wescan.vn/vs/v1/videos?channel=wescan:de3d636e3d6242858b1d9dde19f93080&sort=desc&limit=20&offset=0', true);
        request.setRequestHeader("Authorization", "Bearer " + token);

        request.onload = function() {
            var data = JSON.parse(this.response);
            if (request.status >= 200 && request.status < 400) {
                var i = 0

                data.items.forEach(movie => {
                    i++
                    swiper.appendSlide('<div class="swiper-slide" id=\"slide-' + i + '\">' + movie.title + '</div>')
                    document.getElementById('slide-' + i).style.backgroundImage = "url\(" + movie.thumbnail + "\)";
                })
                swiper.on('click', function() {
                    var index = swiper.clickedIndex
                    openVideo(token, data.items[index])
                });
            } else {
                log("error");
            }
        }

        // Send request
        request.send();

        var swiper = new Swiper(".mySwiper", {
            rewind: true,
            slidesPerView: 3,
            spaceBetween: 30,
            centeredSlides: true,
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
            },
        });
        swiper.keyboard.enable()

    }

    function openVideo(token, movie) {
        hideDiv("home-page");
        displayDiv("video-page");
        document.getElementById("btn-back").onclick = function() {
            backToHome();
        }
        updatePlayerContent(movie);
        var videoTitle = document.getElementById("video-title");
        videoTitle.innerHTML = movie.title

        var request = new XMLHttpRequest();
        request.open('GET', 'https://vs-api.wescan.vn/vs/v1/videos/' + movie.code, true);
        request.setRequestHeader("Authorization", "Bearer " + token);
        request.withCredentials = true;
        request.onload = function() {
            var data = JSON.parse(this.response);

            if (request.status >= 200 && request.status < 400) {
                var src = data.item.src
//                var headers = request.getResponseHeader("set-cookie");              
                var headers = document.cookie;            

                var obj = {
                    "videoThumbnail": movie.thumbnail,
                    "videoUrl": src,
                    "authContent": headers,
                    "mimeType": "application/x-mpegurl"
                };
                readyPlayer(obj)

            } else {
                document.getElementById("log").innerHTML = "error : token: " + token + " | code" + movie.code;

            }
        }
        request.send();
    }

});